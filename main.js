/**
 * Created by Home Sweet Home on 1 Apr 2017.
 */


var express = require("express");

var app = express();

app.use(express.static(__dirname + "/public"));

app.listen(3000, function(){
    console.info("webserver starting on port 3000");
})